FROM ruby:2.6.5-alpine as Builder

RUN apk add --update --no-cache \
    build-base \
    git \
    nodejs-current \
    yarn \
    postgresql-dev \
    sqlite-dev \
    tzdata

WORKDIR /alitwitter
ADD alitwitter/Gemfile* ./
RUN gem install bundler:2.1.4
RUN bundle config --global frozen 1 \
    && bundle config --global without development:test \
    && RAILS_ENV=production bundle install \
    && rm -rf /usr/local/bundle/cache/*.gem \
    && find /usr/local/bundle/gems/ -name "*.c" -delete \
    && find /usr/local/bundle/gems/ -name "*.o" -delete

COPY alitwitter/package.json alitwitter/yarn.lock ./
RUN yarn install
ADD alitwitter/ ./

RUN rm -rf lib/tasks/bootcamp.rake &&\
    RAILS_ENV=production \
    bin/rails assets:precompile

RUN rm -rf node_modules tmp/cache spec config/master.key

FROM ruby:2.6.5-alpine
RUN apk add --update --no-cache \
    tzdata \
    postgresql-client \
    sqlite-dev \
    file

RUN addgroup -g 1000 -S deploy \
    && adduser -u 1000 -S deploy -G deploy
USER deploy

COPY --from=Builder /usr/local/bundle/ /usr/local/bundle/
COPY --from=Builder --chown=deploy:deploy /alitwitter /alitwitter
ENV RAILS_LOG_TO_STDOUT true
ENV RAILS_SERVE_STATIC_FILES true
ENV EXECJS_RUNTIME Disabled
ENV RAILS_ENV=production
WORKDIR /alitwitter

RUN date -u > BUILD_TIME
EXPOSE 3000

CMD ["alitwitter/startup.sh"]
