# Helm Charts for alitwitter
## Description
This is a Helm chart repository for
alitwitter project for Gojek bootcamp 6 devops module

## How to install
This repo are consisted of two different chart configurations:
- The alitwitter chart for the app itself in `alitwitter` folder
- The postgresql chart value file for the database in `postgresql` folder

The postgresql chart is assumed to use `bitnami/postgresql` as its base chart

Before installing, please ensure that you have `Kubernetes` cluster, `kubectl` client
& `Helm` installed and correctly configured. This chart is tested using
`minikube` kubernetes cluster with its default DNS addon and installed locally
using `VirtualBox`. I cannot guarantee using different setup would result in
working solution.

### Steps
1. Ensure you have bitnami `Helm` repository configured by calling `helm repo list`.
    If you have the repo, the output would be something like this:
    ```
    NAME    URL
    ...
    bitnami https://charts.bitnami.com/bitnami
    ...
    ```
    with `...` indicating other repo that you had added

    If you don't have it installed, see [bitnami github page](https://github.com/bitnami/charts)
    for guide to add the repo


2. Install the bitnami/postgresql chart in your kubernetes cluster by using custom value defined in
    `postgresql/values.yaml` and your choice of release name by using the following command:
    ```
    helm install <release-name> bitnami/postgresql -f postgresql/values.yaml
    ```
    take note of your choice of `<release-name>` because we will use it in the next step


3. Ensure the postgresql kubernetes service is up by checking it in the output of `kubectl get service`
    command:
    ```
    user@computer folder % kubectl get service
    NAME                                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
    ...
    kubernetes                           ClusterIP   10.96.0.1        <none>        443/TCP          16h
    <release-name>-postgresql            NodePort    10.102.247.221   <none>        5432:32345/TCP   89m
    <release-name>-postgresql-headless   ClusterIP   None             <none>        5432/TCP         89m
    ...
    ```
    take note of the postgresql service name without the `-headless` prefix since we will set it as
    the database host address for our app pods


3. Edit the value of `database.host` value inside `alitwitter/values.yaml` file with your choice
    of `bitnami/postgresql` release name:
    ```
    ...
    database:
      adapter: postgresql
      host: <release-name>-postgresql # change this value with the service name
      port: 5432
      username: admin
      password: admin
      name: twitter
    ...
    ```


4. Install the alitwitter chart with your choice of release name
    ```
    helm install <app-release-name> ./alitwitter
    ```
    Take note of your choice of `<app-release-name>`


5. Ensure the pods are created by executing `kubectl get pod` and wait until
the setup job had finished running like the example below:
    ```
    user@computer folder % kubectl get pod
    NAME                                READY   STATUS      RESTARTS   AGE
    alitwitter-gif-849d9b64b5-6rq8f     1/1     Running     0          56m
    alitwitter-gif-849d9b64b5-thmxf     1/1     Running     0          56m
    alitwitter-gif-db-setup-job-mtldj   0/1     Completed   0          56m # wait until this pod status is completed
    postgre-db-postgresql-0             1/1     Running     0          98m
    ```


6. Check the app by executing `minikube service <app-release-name>-service` and navigating to `/tweets` address
in the web browser page that will be opened
