require 'rake'
require 'rubocop/rake_task'
require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:tester) do |test|
  test.rspec_opts = ['--format documentation']
end

RuboCop::RakeTask.new(:linter) do |task|
  task.requires << 'rubocop-rails'
  task.options = ['--display-cop-names']
end

RuboCop::RakeTask.new(:linterformat) do |task|
  task.options = ['-a']
end

task :default do
  Rake::Task['tester'].invoke
  Rake::Task['linter'].invoke
end

task :coverage do
  `open coverage/index.html`
end
