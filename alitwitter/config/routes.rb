Rails.application.routes.draw do
  get 'tweet/index'
  post 'tweet', to: 'tweet#create'
  delete 'tweet/:id', to: 'tweet#destroy'
  root 'tweet#index'
end
