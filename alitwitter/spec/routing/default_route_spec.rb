require 'rails_helper'

RSpec.describe 'root routes', type: :routing do
  it 'routes to tweet controller' do
    expect(get('/')).to route_to('tweet#index')
  end
end
