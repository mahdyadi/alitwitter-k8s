require 'rails_helper'

RSpec.describe Tweet, type: :model do
  subject { Tweet.new }

  context 'given valid message to be saved' do
    it 'should be valid' do
      subject.message = 'hello!'

      expect(subject).to be_valid
    end
  end

  context 'given empty message to be saved' do
    it 'should be invalid' do
      subject.message = ''

      expect(subject).to be_invalid
    end
  end

  context 'given message more than 140 length to be saved' do
    it 'should be invalid' do
      subject.message = (0..140).reduce('') do |result|
        result + '0'
      end

      expect(subject).to be_invalid
    end
  end
end
