require 'rails_helper'

RSpec.describe 'tweet/index.html.erb', type: :view do
  context 'given no tweets data sent' do
    context 'when rendered' do
      it 'should display no post notification' do
        assign(:tweets, [])

        render

        expect(rendered).to match(/0 tweets/)
      end
    end
  end

  context 'given any number of tweets' do
    context 'when rendered' do
      it 'should display alitwitter header text' do
        assign(:tweets, [])

        render

        expect(rendered).to match(/AliTwitter/)
      end

      it 'should display form container for tweet input' do
        assign(:tweets, [])

        render

        expect(rendered).to match(/<form.*>/)
      end

      it 'should display textarea input for tweets' do
        assign(:tweets, [])

        render

        expect(rendered).to match(/<textarea.*>/)
      end

      it 'should display submit button for posting tweets' do
        assign(:tweets, [])

        render

        expect(rendered).to match(/<input.*type="submit".*>/)
      end

      it 'should point to /tweet for submission' do
        assign(:tweets, [])

        render

        expect(rendered).to match(%r{<form action="/tweet".*method="post".*})
      end
    end
  end

  context 'given nonzero number of tweets' do
    context 'when rendered' do
      it 'should include those tweet messages' do
        first_tweet = Tweet.new
        first_tweet.message = 'hello'
        second_tweet = Tweet.new
        second_tweet.message = 'world'
        assign(:tweets, [first_tweet, second_tweet])

        render

        expect(rendered).to match(/hello/)
        expect(rendered).to match(/world/)
      end

      it 'should include delete link with containing tweet id' do
        first_tweet = Tweet.new
        first_tweet.id = 1
        first_tweet.message = 'hello'
        second_tweet = Tweet.new
        second_tweet.id = 2
        second_tweet.message = 'world'
        assign(:tweets, [first_tweet, second_tweet])

        render

        expect(rendered).to match(%r{href="/tweet/1"})
        expect(rendered).to match(%r{href="/tweet/2"})
      end
    end
  end
end
