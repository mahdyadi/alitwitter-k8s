# AliTwitter
## Description
A Twitter-like app that can do the following:
[x] show user tweet
[x] post user tweet
[x] delete user tweet

Created in accordance to Gojek bootcamp 006 problem statement
at the end of the Core Engineering module.

## Dependencies
You need the following tools and packages installed in your
machine:
- Ruby 2.6.5
- Bundler 2.1.4
- Node 13.11.0
- Yarn 1.22.4
- Rails 6.0.2.1
- Postgres 10

## Development setup
Configure the following environment variables to ensure the app would run:
```
DATABASE_ADAPTER=postgresql     #database type: postgresql or sqlite3(default)
DATABASE_USERNAME=rails         #username for the database role
DATABASE_PASSWORD=xxx           #password for the used role
DATABASE_HOST=localhost         #host address of the database location
DATABASE_PORT=5432              #port opened of the database location
DATABASE_NAME=alitwitter_rails  #name of the database
DATABASE_TIMEOUT=5000           #timeout of the connection
PORT=3000                       #port of the application when running
```
Make sure the user role used specified in the environment variable have CREATEDB privilege
if you plan to use postgresql as database which will be used by the app when preparing your
database schema (when `rails db:setup` command called).

You can use `.env` file in dev environment to set those env variable, as long as it is located
in this app root directory.

## Deployment setup
TBD

## Run Test
using rspec & rubocop
```bash
bundle exec rake
```

to open the coverage result in browser
```bash
bundle exec rake coverage
```

## Run Instruction
Do database setup first if you haven't do it
```bash
bundle exec rake db:setup
```

Do the DB migration
```bash
bundle exec rake db:migrate
```

In development environment, execute the server using rails:
```bash
bundle exec rails server
```

Then open `http://localhost:3000/` to
interact with the application
