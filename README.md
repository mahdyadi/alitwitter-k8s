# AliTwitter App with Container based infrastructure
## Description
This is a project repository for Gojek's bootcamp 006
devops module. Included in this repository are:

- Web application (alitwitter) with accompanying Gitlab CI script

- Vagrant boxes setup for Gitlab Runner

- Ansible playbooks for configuring VM gitlab runner

- Helm charts for kubernetes deployment

The aim of this project is to demonstrate CI/CD flow using container-based
infrastructure, from code change to deployed app.

Infrastructure setup is automated provided you have the necessary tool installed.
For more information, you can refer to the `Infrastructure setup` part below.

Continuous Integration(CI) and Continous Deployment(CD) flow on top of the prepared
infrastructure is achieved using Gitlab CI features

## Important Notice
Currently, the deployment step always fail because of unresolved issue
So the only pipeline that works is setting up the gitlab-runner and
testing phase

## Project organization
- The web application folder is located in `alitwitter` directory
- Provisioning playbooks is grouped inside `playbooks` directory
- Helm charts for docker deployment to kubernetes cluster is located in `charts` directory

## Usage requirement
This project was developed with help of the following tools:
- `Ansible 2.9` or greater
- `Vagrant 2.2.7`
- `VirtualBox` as Vagrant provider
- `Minikube` as local kubernetes cluster
- `kubectl` as local kubernetes cluster API client

You can install all above tools in your local computer
to use this project.

This project is tested and developed on MacOS 10.15.3 Catalina. Compatibility
when used with different setup is not guaranteed.

## How to use
Please follow all instruction described below step-by-step to ensure
a working infrastructure and application is achieved

### Gitlab Repository Setup

Create new git repository and checkout all files and folder in this project
```
$ git init
$ git add .
$ git commit
```

Then you can create new Gitlab repository and get the repository's private runner
`registration token` via the Gitlab UI. Copy and paste the value into the runner
provisioning playbook `gitlab_regis_token` variable in `playbooks/provision_runner.yml`
```yaml
---
- name: Setup shell runner
  hosts: gitlab_runner
  ...
  vars:
    ...
    gitlab_regis_token: # place the token here
  roles:
    ...
```

You also need to set Gitlab CI/CD Variables for the following key/value:

| Variable Key               | Variable value                                                       |
|----------------------------|----------------------------------------------------------------------|
| `RAILS_MASTER_KEY`         | Value of from `master.key` file, see alitwitter readme for more info |
| `RUNNER_DATABASE_USERNAME` | Test database username used in the runner                            |
| `RUNNER_DATABASE_PASSWORD` | Test database user passsword used in the runner                      |

both `RUNNER_DATABASE_USERNAME` and `RUNNER_DATABASE_PASSWORD` value can be copied from
`postgresql.user` and `postgresql.user_password` value from `playbooks/inventory/group_vars/all.yml`,
which is the credentials of database of the deployment environment. Currently no distinction is made
for test database user and production database user identity because both credentials would still
be exposed in this project.

An example and matching `master.key` file for current app is included in the root of the project
named `default.master.key`. You can use this master key if you did not want to generate
a new one.

Add the repository into this project's git repository remote as origin or other
remote name of your choice

### Infrastructure setup
1. Setup virtual machine boxes/instance.

    Make sure Vagrant is installed and VirtualBox is set as its provider(or other provider of your choice),
    then initialize Vagrant boxes using the `Vagrantfile` provided by this project in project root directory.
    It is highly advised that you do this step in the project root directory.

    - `gitlab-runner` : Machine to be set as Gitlab Runner instance to enable CI/CD flow

    example commands:
    ```
    $ cd < this project path >
    $ vagrant up
    ```

2. Setup playbook configuration for Ansible
    Get the ssh key for each machines/boxes by calling `vagrant ssh-config` and take note of
    the IdentityFile locations

    example:
    ```
    $ vagrant ssh-config
    Host gitlab-runner
      ...
      IdentityFile /Users/drianka/devops_vm_assessment/.vagrant/machines/database/virtualbox/private_key
      ...
    ```

    Then modify the `ansible_ssh_private_key_file` for gitlab-runner host in `playbooks/inventory/hosts.yml` file based on the found IdentityFile

    example:
    ```yaml
    ---
    all:
      hosts:
        gitlab_runner:
          ...
          ansible_ssh_private_key_file: # place gitlab-runner IdentityFile path here
          ...

    ```

3. Setup service account of cluster for connection
   To enable connection between Gitlab runner and the cluster, we need to make sure
   kubectl that will be installed in the runner can connect to the cluster.

   To achieve that, we need to create a service account and get the service account credentials
   For example, if we name our service account `deploy`, we create it with:
   ```
   $ kubectl create sa deploy
   ```

   Then we set its role with
   ```
   kubectl create clusterrolebinding deploy --clusterrole cluster-admin --serviceaccount default:deploy
   ```

   Don't forget to get the actual cluster server location, you can use `kubectl config view`
   ```
   $ kubectl config view
   apiVersion: v1
   clusters:
   - cluster:
       ...
       server: https://192.168.99.101:8443 #take note of this address
     name: minikube
   contexts:
     ...
   users:
     ...
   ```

   Then we need to get its secret name to get the credentials
   ```
   kubectl get sa deploy -o jsonpath='{.secrets[0].name}' > secret_name.txt
   ```

   Then we need to get its API token
   ```
   kubectl get secret $(cat secret_name.txt) -o jsonpath='{.data.token}'|base64 --decode > api_token.txt
   ```

   Lastly we need to get its certificate
   ```
   kubectl get secret $(cat secret_name.txt) -o jsonpath='{.data.ca\.crt}'|base64 --decode > api_cert.crt
   ```

4. Populate variable for kubectl connection

    Using data taken in the previous step, populate `deploy` group variable in `playbooks/inventory/group_vars/all.yml`
    ```yaml
    ...
    deploy:
      certificate_file: #put filename of the certificate file
      cluster_name: #modify cluster name if you want
      cluster_ip: #put value of cluster server here
      context: #modify context name if yu want
      sa_api_token_file: #put filename of the api token
      sa_name: #put the service account name here
    ...
    ```

    Then copy/move both the certificate file and api token file
    to `playbooks/roles/configure_cluster_connection/files/`


3. Setup boxes machine using provisioning playbooks
    Make sure Ansible is installed, and you can execute the main provisioning
    playbook `playbooks/provision_runner.yml` with inventory hosts file `playbooks/hosts.yml`

    example commands:
    ```
    $ ansible-playbook playbooks/provision_runner.yml -i playbooks/hosts.yml
    ```

    Wait until the setup is done. Make sure you have stable internet connection
    to ensure no error raised by inability to fetch packages from the remote source

### Triggering CI/CD Flow

All step of CI/CD flow is written in the `.gitlab-ci.yml` file in the project root
directory.

You just need ensure you private runner is ready to accept jobs and this project
remote git repository is already set

Push the project to the remote repository to trigger test, build, and deployment
step
