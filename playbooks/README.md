# Ansible Playbooks for Infrastucture Setup

## Description
This project contains playbook for setting up various machine roles:
- Gitlab Runner

## Highlight
- Most of the provisioning roles uses module-specific functionality rather
  than shell & command module to ensure idempotency is working as intended
- Roles that did not use moddule with idempotency suppport were given
  conditional guard on wheter it should be skipped or not when possible
- Gitlab runner provision playbook includes installing both postgresql and
  sqlite3 to preserve the Gemfile.lock requirement and maintains flexibility
  for the future

## How to use
To provision gitlab-runner VM, just execute
```
$ ansible-playbook <provision_script> -i inventory/hosts.yml
```

## Configurable variable
You can configure variables in `inventory/group_vars` and in each provision playbooks
- in `all.yml` you can configure ruby, node, and postgresql
